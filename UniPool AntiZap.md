---


---

<ol>
<li>Once the user flips the card (toggle etc), the user will see the option to liquidate her liquidity token</li>
<li>Once the user clicks “Use this AntiZap”, the modal should popup</li>
<li>The input field should be deactivated, unless the user connects the wallet.  Connection will help us determine the Qty of liquidity tokens that the user has which can be shown as <code>max value</code> below the input field</li>
<li>Once the user inputs the QTY — we can (I say again, we can) simulate the tx and let them know how much ETH is expected to come out.  This is an involved process and we will have to use Uniswap API for this.  Suhaiel and Tosh both will be needed for this.</li>
<li>Once the user confirms the QTY, we will do a web3js to the ERC20 contract address [for this we will need the ABI of the ERC20 address, which means, we will have to use EtherScan to fetch it].  The web3js will be <code>approve</code>.  This tx will be approving DeFiZap Contract to use the funds</li>
<li>The User will then Sign Tx 2 (which will be inside of DeFiZap Contract) which we can name <code>LetsGetETHOut</code>.  This will have a parameter of the QTY from Step 5 above and <em>most likely</em> a couple of more things.  {I will be working on this}.</li>
<li>The SmartContract will then call the transferFrom function in the Uniswap Exchange Contract, to transfer the Token to itself; This is a ridiculous step.  It is needed since the next step requries the address initiating it to be the owner of the Liquitiy Tokens [see step 8].</li>
<li>Once that is done, the SC will then call the fx <code>removeLiquidity</code> with the necessary parameters (which include the QTY).</li>
<li>Upon completion of Step 8, the SC will be in possession of the tokens and the ETH</li>
<li>Upon compeletion SC will re-talk to the UniSwap or we can go to Kyber and get the tokens converted back to ETH [this is also a thing, cause Kyber does not support any and every coin, eg sETH]</li>
<li>Once the SC is in possession of the ETH (after converting), the SC will do a <code>.send</code> or <code>address.call()</code> {this needs to be tested due to gas restrictions} and send the ETH out.</li>
</ol>
<p><strong>NOTE</strong>: step 4 is optional and the one that can delay the process.<br>
Also, if someone has suggestions for Step 7 that will be helpful.  The reason is that mi<em>e</em>s can f**k this function to cause a big havoc (ie a mix and match, if multiple people are exeucting the AntiZap at the same time).</p>
<blockquote>
<p>Written with <a href="https://stackedit.io/">StackEdit</a>.</p>
</blockquote>

