---


---

<p>ETH to Depot.sol | fx call <code>exchangeEtherForSynths</code><br>
This will give me the SUSD back</p>
<p>PoolTogether</p>
<ol>
<li>Approve the PoolTogether Contract</li>
<li>Transfer DAI with the depositPool function</li>
<li>Pooltogether does not give back any ERC20 token</li>
<li>It just takes the DAI and invests in CompoudDai</li>
<li>Thus, only the address which transfers the DAI will be a participant into the pool</li>
<li>Somewhere in the code of Pooltogether there is a restriction/modifier with the name <code>isContract</code></li>
<li>The main point of this function is to make sure that the msg.sender is a contract or an <code>externally owned account</code></li>
<li>Thus, giving me a hint, at the time of writing this, that even if I do a middleware Smart Contract for participating in the pool, I do not think it will work. May be!!!  Cause, if the fx <code>depositPool</code> is called by a smartcontract, it may be reverted</li>
<li></li>
</ol>

