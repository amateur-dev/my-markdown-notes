---


---

<ol>
<li>Obama comes and transfers ETH for USD 500</li>
<li>ERC20 token (name: DEFI Token) to be minted and sent by the ERC20 SmartContract</li>
<li><em>the issue to be resolved</em> question is: how many DEFI Token to be minted and sent for every single USD that is transfered by Mr Obama?</li>
</ol>
<p>Eg</p>
<ul>
<li>if you send 1 ETH to the KyberNetwork Smart Contract - you will get back 500 KNC Token (KNC token is an ERC20 Token)</li>
<li>if you send 1 ETH to CryptoKitty SmartContract - you will get back 2 CrytoKitty Token</li>
<li>if you send 1 ETH to ChainLink SmartContract - you will get back 100 Link Tokens</li>
<li>question - if you send 1 ETH to DEFI SmartContract - how many DEFI tokens will you get back?</li>
</ul>

