---


---

<h2 id="quick-comments-on-the-contract-created-by-anello">Quick Comments on the Contract Created by Anello</h2>
<ol>
<li>Need to update for latest solidity versioning</li>
<li>Need to make sure that the constructor fx sets the <code>owner</code> and the <code>user</code> as the message.sender</li>
<li>Need 2 modifiers <code>onlyOwner</code> and <code>onlyUser</code></li>
<li>Need to be a little more advanced</li>
</ol>

