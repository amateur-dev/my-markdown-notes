---


---

<p><strong>Cash Tx</strong></p>
<p>pay USD 180 --&gt; you get 1 ETH</p>
<p><strong>Margin Tx</strong></p>
<ul>
<li>pay USD 180 (as collateral)</li>
<li>you say that I want to execute a 4x Margin Tx</li>
<li>because of this, you will be able to buy <strong>4</strong> ETHs (as compared to just 1 ETH in the cash tx)</li>
<li>if there is a drop in the price of the ETH, a margin call will me made to you
<ul>
<li>you will have to deposit the margin call</li>
<li>if you fail to deosit the margin call, your ETH will be sold and the broker will make sure that his interest is protected</li>
</ul>
</li>
</ul>
<p><strong>BZX</strong></p>
<p>it does exactly was a margin tx is and does the following additional:</p>
<ul>
<li>it tokenises the investment as an ERC20 token</li>
<li>that token can then be traded with anyone on the blockchain</li>
</ul>

