---


---

<h1 id="defi-investment-strategies-app---manners-in-which-we-can-enhance-the-app">DeFi Investment Strategies App - manners in which we can enhance the app</h1>
<p>at present the app shows you multiple strategies for investment purposes<br>
the objective is that that user should be able to deploy these investment strategies with the click of one button</p>
<p><img src="https://lh3.googleusercontent.com/rV1rhfkiRGIc9BQ7Wdn5XGwlnlNSzCSHzzcFwLYYpchOgTSwuzIjM9LQ9FQORwmZSDi54GBiBFU" alt="Conservative Strategy Screenshot"></p>
<p>At this stage, there is a button	<code>deploy strategy</code></p>
<h2 id="options">Options</h2>
<p>Following are the options that we have in which we can implement this, please note, we are discussing this in the context of the consverative strategy:</p>
<p><strong>(A)</strong> <strong>Similar to Dharma v2</strong> (also somewhere close to Fulcrum) - the app acts only as an interface and just assits in doing the transaction.  This will inject web3 tx:</p>
<ol>
<li>Use Kyber to convert any ERC20 token to DAI / ETH / wETH</li>
<li>Inject web3 to now transfer the DAI / wETH to transact with Sets or cDAI</li>
</ol>
<p><em>to note</em>: web3js should specifiy the gas price.</p>
<p><em>Benefits:</em></p>
<ul>
<li>Non custodial through out</li>
<li>the user has 100% control on the assets generated</li>
<li>the user can independently liquidate the position, without the need of the app</li>
</ul>
<p><em>Loss:</em></p>
<ul>
<li>Multiple transactions visibile to the user, bad UX</li>
</ul>
<p><strong>(B)</strong> <strong>Similar to InstaDapp and NUO</strong>  - a smart contract is deployed for every new investment:</p>
<p><img src="https://lh3.googleusercontent.com/gLuNljSPyyO5_Tl9TllLDz3CjR8dvH5s51YZOWKaAhK4CCV2zVxpAvbkFu21u36-Xd-PSGpQDx0" alt="Nuo interface"></p>
<ol>
<li>User sends the necessary ETH  to the SmartContract</li>
<li>The SmartContract does all the work and holds the investment</li>
<li>The SmartContract can only be controlled by the user’s wallet<br>
Eg: <a href="https://etherscan.io/tx/0x7348c6891f724ce656761af38ea7ad0e71452fe3dbc620b79cd19c272e7f3b3c">Investment in Compound using InstaDapp</a></li>
</ol>
<p><em>Benefits:</em></p>
<ul>
<li>easy to execute</li>
<li>no multiple transactions visible to the user and hence better UX</li>
<li>one time gas to be paid by the user on sending funds</li>
<li>one time gas to be paid on liqidating funds</li>
</ul>
<p><em>Loss:</em></p>
<ul>
<li>the investment in sets and dai are bundled in the SC, user cannot choose to liquidate any one of them</li>
<li>the user always is dependent on our app to liquidate the position, as he or she will find it difficult to interact with the contract otherwise</li>
</ul>
<p><strong>©</strong> <strong>Tokenising the investment</strong><br>
This strategy is based on what <em>Compound</em> and <em>bzx</em> do; the user’s investment is tokenised and it is then issued to the wallet of the user.</p>
<p><em>Benefits:</em></p>
<ul>
<li>one of the most mordern way of doing things (ERC20 token)</li>
<li>possbility of token being listed and traded on other DEX (eg Kyber)</li>
<li>no multiple transactions visible to the user and hence better UX</li>
<li>one time gas to be paid by the user on sending funds</li>
<li>one time gas to be paid on liqidating funds</li>
</ul>
<p><em>Loss:</em></p>
<ul>
<li>the investment in sets and dai are bundled in the SC, user cannot choose to liquidate any one of them</li>
<li>the user always is dependent on our app to liquidate the position, as he or she will find it difficult to interact with the contract otherwise</li>
</ul>

